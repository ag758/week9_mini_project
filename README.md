# Streamlit App with a Hugging Face Model

This project is a Streamlit web application that connects to an open source Language Model (LLM) provided by Hugging Face. The model is deployed via Streamlit or another service and can be accessed through a web browser. This has been hosted on EC2 server. 

## Requirements

- Create a website using Streamlit
- Connect to an open source LLM (Hugging Face)
- Deploy the model via Streamlit or another service (accessible via a browser)

## Connection to Open Source LLM:
1. Used transformer library from Hugging Face.

`from transformers import pipeline`


# Creating a Streamlit web app
The website is a summarization application that utilizes the Transformer library from Hugging Face. It allows users to input a paragraph of text and generates a summary of the input using the Hugging Face model. The application is built using the Streamlit framework, providing a user-friendly interface accessible through a web browser.

The website is hosted on Streamlit, a platform that allows developers to easily deploy and share their machine learning models and data science projects. Streamlit provides a user-friendly interface that makes it easy for users to interact with the summarization application.

In addition to Streamlit, the application is also hosted on Amazon EC2 (Elastic Compute Cloud). EC2 is a part of Amazon's cloud-computing platform, Amazon Web Services (AWS), and provides scalable computing capacity in the cloud. This allows the application to handle a large number of users and requests without any performance issues.

The combination of Streamlit and EC2 makes the application highly accessible and scalable, ensuring a smooth user experience regardless of the number of users or the size of the input data.

## Usage

1. Clone the repository:

    ```shell
    git clone https://gitlab.com/ag758/week9_mini_project.git
    ```

2. Install the required dependencies:

    ```shell
    pip install -r requirements.txt
    ```

3. Run the Streamlit app:

    ```shell
    streamlit run app.py
    ```

4. Access the web app in your browser at `http://localhost:8501`.

## Website Functioning:
#### text summarization App hosted on Streamlit
![alt text](Images/text_summarization_image.png)
#### text summarization Result
![alt text](Images/Text_Summarization_result.png)

#### text summarization hosted on EC2
![alt text](Images/Ec2_st_app.png)


#### text summarization hosted on EC2
![alt text](Images/EC2.png)




## Documentation

For detailed documentation on how to use the app and connect to the open source LLM, please refer to the [docs](docs/README.md) directory.



